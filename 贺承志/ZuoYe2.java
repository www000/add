/*作业2：子类实例化过程
  1、学生类继承人类

  2、实现学生类构造函数

  3、实现人类构造函数

  4、在主类中创建学生对象，验证子类和父类构造函数的调用顺序*/


public class ZuoYe2 {

	public static void main(String[] args) {
		//定义一个新的学生对象
		Students stu  = new Students("小胖",20);
		//调用函数
		stu.introduce();
		stu.Wc();

	}

}
//人类
class Persons{
	//属性
	String name ;
	int age ;
	//构造
	Persons(){
		System.out.println("一个人类诞生了");
	}
	Persons(String name , int age){
		this();
		this.name = name ;
		this.age = age ;
	}
	//行为 自我介绍
	void introduce(){
		System.out.println("我的名字："+this.name+"我的年龄："+this.age);
	}
	
}
//学生类继承扩展人类
class Students extends Persons{
	//构造
	
	Students(String name , int age){
		super (name ,age);
		System.out.println("一个学生诞生了");
	}
	//行为
	void Wc(){
		System.out.println("正在上厕所");
	}
	
}

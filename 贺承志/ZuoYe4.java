/*作业4：对象向上转型
  1、学生类继承人类

  2、在主类中创建学生类对象

  3、将学生类对象赋值给人类引用

  4、通过人类引用调用自我介绍方法*/


public class ZuoYe4 {

	public static void main(String[] args) {
       //创建一个学生对象
		Student4 stu = new Student4("小何",20,"男");
		Person3 p = stu ;
		stu.introduce();
	}

}
class Person3{
	String  name ; 
	int  age ;
	String sex;
	//构造
	Person3(String name , int age ,String sex){
		this.name = name ;
		this.age  = age   ;
		this.sex = sex ;
		
	}
	void introduce(){
		System.out.println("我的姓名：  "+this.name+"我的年龄：  "+this.age+"我的性别： "+this.sex);
	}
}
//学生类 继承 人类
class Student4 extends Person3{
	 //构造
	Student4(String name , int age ,String sex){
		super(name,age,sex);
				
	}
	
}
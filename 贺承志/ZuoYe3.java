/*作业3：子类复写父类方
  1、学生类继承人类

  2、学生类复写自我介绍方法，增加学号介绍

  3、尝试在复写的自我介绍方法中，先调用父类自我介绍方法介绍姓名、年龄，再介绍学生学号。*/ 
public class ZuoYe3 {

	public static void main(String[] args) {
		//创建对象
	Student3 stu = new Student3("贺承志",20,"贵安","14350306");
		stu .introduce();
		stu .number(); 
	}

}
//人类
class Person{
	//属性
	String name ;
	int    age  ;
	String address;
	//构造
	Person(String name , int age, String address ){
		this.name = name ;
		this.age = age ;
		this.address = address;
		
	}
	//行为方法 自我介绍
	void introduce(){
		System.out.println("我的姓名是：  "+this.name+"我的年龄是：  "+this.age+"我的地址是：  "+this.address);
	}
	
}
//学生类
class Student3 extends Person{
	//属性
	String id ;
	 //构造
	Student3(String name , int age ,String address,String id){
		super(name,age,address);
		this.id = id;
	}
	//行为方法
	void introduce(){
		System.out.println("我的姓名是：  "+this.name+"我的年龄是：  "+this.age+"我的地址是：  "+this.address);
	}
	void number(){
		System.out.println("我的学号是："+this.id);
	}
}
/*作业1：学生类继承人类
  1、设计人类
     属性：姓名、年龄
     行为：思考、吃饭、自我介绍

  2、设计学生类,继承人类
	   增加属性：学号
     增加行为：上课、考试
  
  3、在主类中创建学生对象， 调用其继承来的行为和增加的行为*/

public class Test {

	public static void main(String[] args) {
		Person1 p = new Person1("小贺",20);
		p.think();
		p.eat();
		p.introduce();
        Student1 stu = new Student1("小贺先生",5,"74110");
        stu.think();
        stu.eat();
        stu.inclass();
        stu.test();
        stu.introduce();
        stu.number();
	}

}
//人类
class Person1{
	String name ;
	int    age  ;
	private String id;
	Person1(){
		System.out.println("一个人类诞生了");
	}
	Person1(String name , int age){
		this();
		this.name = name ; 
		this.age  = age  ;
		
	}
	void think(){
		System.out.println("正在思考");
	}
	void eat(){
		System.out.println("正在吃饭");
	}
	void introduce(){
		System.out.println("我的名字"+this.name+"我的年龄"+this.age);
	}
}
//学生类
class Student1 extends Person1{
	
	String id ;
	
	Student1 (){
		System.out.println("一个学生诞生了");
		
	}

	Student1(String name , int age , String id){
		this();
		this.name = name;
		this.age = age;
		this.id = id ;
		
	}
	
	void inclass (){
		System.out.println("正在上课");
	}
	void test(){
		System.out.println("正在考试");
	}
	void number(){
		System.out.println("我的学号是："+id);
	}
}
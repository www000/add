//人类
public class Person {

	//属性
	String name;
	int    age;
	
	//构造
	Person(String name, int age){
		System.out.println("一个人诞生！");
		this.name = name;
		this.age = age;
	}
	
	//行为
	void eat(){
		System.out.println(this.name + "正在吃饭！");
	}
	
	void sleep(){
		System.out.println("躺在床上舒服的睡！");
	}
	
	void introduce(){
		System.out.println("名字是：" + this.name);
		System.out.println("年龄是：" + this.age);
	}
}

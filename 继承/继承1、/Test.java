
public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//创建学生对象
		
		//1、调用父类构造函数=>子类的构造函数
		//2、默认调用父类的无参构造函数
		//3、子类通过super（参数）显式调用父类的构造函数
		
		Student st = new Student("张三", 20, "贵州", "1011");
	
		
		//st.think();
		st.introduce();
		
		//st.study();
		st.intoduceStudent();
		
		/*
		//创建人对象
		Person pr = new Person("王五", 40, "贵安");
		pr.think();
		pr.introduce();
		
		
		//pr.study();
		*/
		
		//创建老师对象
		//Techer tc = new Techer("曾老师", 40);
		
		/*
		tc.think();
		tc.introduce();
		*/
		
	}

}

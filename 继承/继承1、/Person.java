
//人类
public class Person {

	//属性
	String name;
	int    age;
	String addr; //地址
	
	//构造
	Person() {
		System.out.println("一个人诞生了！");
	}
	
	Person(String name, int age, String addr){
		this();//调用无参构造函数
		
		this.name = name;
		this.age = age;
		this.addr = addr;
	}
	
	//行为
	
	//思考
	void think(){
		System.out.println(this.name + "正在思考");
	}
	
	//自我介绍
	void introduce(){
		System.out.println("我的名字:"+ this.name);
		System.out.println("我的年龄:"+ this.age);
		System.out.println("我的地址:"+ this.addr);
	}
}

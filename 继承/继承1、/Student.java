//学生类: 继续=>扩展


//1、子类通过extends关键字继承父类
//2、子类一旦继承类父类，就拥有父类的所有属性和行为
//3、子类还可以扩展自身的属性和行为
//4、继承的好处是减少重复代码，更改时只需要更改一份

public class Student extends Person{
	//属性
	String id; //学号
	
	//构造
	public Student(String name, int age, String addr, String id) {
		// TODO Auto-generated constructor stub
		
		//先初始化从父类继承来的属性
		super(name, age, addr);
		
		//再初始化自身扩展的属性
		this.id = id;
		
		System.out.println("一个学生诞生了！");
		
		/*
		this.name = name;
		this.age = age;
		*/
		
	}
	
	//行为
	
	//学习
	void study(){
		System.out.println(this.name + "正在学习！");
	}
	
	//学生自我介绍
	void intoduceStudent(){
		
		//调用父类自我介绍
		introduce();
		
		//介绍自身扩展的属性
		System.out.println("我的学号是：" + this.id);
	}
}

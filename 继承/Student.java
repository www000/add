//学生=>继承人类  extends：扩展
public class Student extends Person {
	
	//属性
	String id; //学号
	
	//构造
	Student(String name, int age) {
		super(name, age);//调用父类构造函数
	}
	
	Student(String name, int age, String id) {		
		this(name, age);//调自身的重载构造函数
		this.id = id;
	}
	
	//行为
	void study(){
		System.out.println(this.name + "正在学习");
	}
	
	@Override
	void introduce(){
		super.introduce();//调用父类方法
		System.out.println("学号是：" + id);
	}
}


public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//创建学生对象
		Student st = new Student("大张", 7,"007");
		
		//增加的方法
		st.study();
		
		//继承的方法
		st.eat();
		
		//复写的方法
		st.introduce();
		
		System.out.println("-------------------");
		
		//对象的转型：子类对象和父类对象之间的转换
		Person pr = new Person("小张", 3);	
		pr.eat();
		//pr.study();
		
		pr.introduce();//人自我介绍，介绍的是人
		
		System.out.println("-------------------");
		
		//将子类的对象转换成父类的对象
		
		//将子类的对象上向转型给父类的引用
		//1、子类增加的行为不能使用
		//2、子类复写的行为复写使用
		//3、子类继承的行为正常使用
		
		pr = st; //向上转型，学生是一个人
		
		//子类继承的行为
		pr.eat();
		
		//子类扩展的行为
		pr.introduce();//人自我介绍，介绍的是学生
		
		//子类增加的行为
		//pr.study();//人没有学习的方法
		
		System.out.println("-------------------");
		
		//怎么让pr指向的对象学习
		st = (Student)pr;//向下转型, 人不一定是学生, 需要强制转换
		st.study();

	}

}
